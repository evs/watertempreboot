# Sources & Crédits Moritz Feigl et al.
## La publication dans une revue
http://doi.org/10.5194/hess-25-2951-2021

## Les publications dans des dépôts data/code
https://github.com/MoritzFeigl/wateRtemp/
https://doi.org/10.5281/zenodo.5361142

# Historique
Essai infructueux d'utilisation du paquet dans le cadre d'un stage EVS-Isthme, qu'il s'agisse des données de test fournies avec le paquet ou des données internes EVS-Isthme

# Motivations
1. apprendre à débugger
1.1 débugger permet/implique de comprendre le code (!)
2. apprendre à packager
3. reproductibilité / distribution cross OS / pérennité

# Outils
- `Rstudio`: IDE
- `Gitlab`: partage et versionnage 
- `R{renv}`: version de R et des paquets
- `Docker`: OS & Paquets du Système

# TODOs
## Stratégies
1. (re-)créer un environnement contemporain de la publication du paquet `wateRtemp` (~2021)
2. créer un environnement récent (2024) en adaptant / débuggant le paquet `wateRtemp`

## étapes
- [x] importer une image Docker/Podman up-to-date
- [ ] installer les dépenddances de `wateRtemp`
- [ ] débugger/tester `wateRtemp` sous cet environnement
- [ ] créer/modifier l'image

# Documentation
[Débugger du code R avec RStudio](https://support.posit.co/hc/en-us/articles/200713843-Debugging-R-code-with-the-RStudio-IDE#introduction)

